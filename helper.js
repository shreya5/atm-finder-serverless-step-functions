const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

const algoliasearch = require('algoliasearch');
const algoliaClient = algoliasearch('XXX', 'XXX');
const algoliaIndex = algoliaClient.initIndex('locations');

const googleApiClient = require('@google/maps').createClient({
  key: 'XXX'
});

const SlackWebhook = require('slack-webhook');
const slack = new SlackWebhook('https://hooks.slack.com/services/T6N06LH43/B6N0GE8BV/XXX');

exports.getData = () => {

  const params = {
    TableName : "ATM_LOCATIONS"
  };

  return new Promise((resolve, reject) => {

    docClient.scan(params, function(err, data) {
      if (err) {
        reject(err);
      }
      else {
        resolve(data);
      }
    });

  });
};

exports.startStateMachine = location => {

  console.log('in startStateMachine ' + location.locationId);
  const params = {
    stateMachineArn: "arn:aws:states:us-east-1:775695676505:stateMachine:geo-choice-algolia-fail",
    input: JSON.stringify(location)
  };

  const stepfunctions = new AWS.StepFunctions();
  stepfunctions.startExecution(params, function(err, data) {

    if (err) {
      console.log("There was a error");
      console.error(err);
    }
    else  {
      console.log('StateMachine start was successfull for ');
      console.log(data);
    }
  });

};

exports.findGeoCode = addressText => {

  return new Promise((resolve, reject) => {

    googleApiClient.geocode({
      address: addressText
    }, (err, response) => {

      if (err) { reject(err);}

      if (response.json.results.length > 0) {
        const geometry = response.json.results[0].geometry;
        resolve(geometry.location);
      } else {
        resolve(null);
      }

    });

  });

};

exports.pushToAlgolia = location => {
  return algoliaIndex.addObject(location);
};

exports.sendToSlack = message => {
  slack.send(message)
    .then(data => {
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
};

exports.removeFromAlgolia = locationId => {
  return algoliaIndex.deleteObject(locationId);
};

exports.findLocationsByGeoCodes = geocodes => {

  return algoliaLocationsIndex.search({
    aroundLatLng: `${geocodes.lat}, ${geocodes.lng}`,
    aroundRadius: 10000  //roughly 6 miles
  });

};